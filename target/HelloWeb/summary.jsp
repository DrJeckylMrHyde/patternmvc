<%@ page import="java.util.List" %>
<%@ page import="pl.javastart.models.CartItem" %>
<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Podsumowanie zakupów</title>
</head>
<body>
<h1>Podsumowanie zakupów</h1>
<h2>Lista zakupów:</h2>
<ol>
    <%
        List<CartItem> cart = (List<CartItem>) request.getAttribute("cart");
        for (CartItem cartItem : cart) {
    %>
    <li>
        <%=
            String.format("%s Cena oryginalna: %.2f, cena po rabacie: %.2f",
                    cartItem.getProduct(),
                    cartItem.getOriginalPrice(),
                    cartItem.getDiscountPrice())
        %>
    </li>
    <%
        }
    %>
</ol>
<h2>Suma przed rabatem: <%= request.getAttribute("originalSum") %></h2>
<h2>Suma po rabacie: <%= request.getAttribute("discountSum") %></h2>

</body>
</html>
